CREATE TABLE Card (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    card VARCHAR(20) NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP NOT NULL
);