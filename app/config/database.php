<?php

class Database
{
    private $host = "db_mysql";
    private $database_name = "phprest";
    private $username = "user";
    private $password = "password";
    private $options = array(PDO::MYSQL_ATTR_FOUND_ROWS => true);
    public $conn;
    public $error;
    public function getConnection()
    {
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password, $this->options);
            $this->conn->exec("set names utf8");
        } catch(PDOException $exception) {
            $this -> error = $exception->getMessage();
            echo "Database could not be connected: " . $exception->getMessage();
            return $this -> error;
        }
        return $this->conn;
    }
}
