<?php

    function getErrorMessage($errorInfo)
    {
        switch ($errorInfo[1]) {
            case 1062:
                return 'Запись уже существует.';
            case 1074:
                return 'Слишком длинная запись.';
            default:
                return "Ошибка базы данных.";
        }
    }

    function returnMessage($status, $message)
    {
        return json_encode(
            array($status => $message),
            JSON_UNESCAPED_UNICODE
        );
    }

    // function limit(){
    //     return $this -> pageSize;
    // }

    function offset($page)
    {
        return ($page - 1) * 5;
    }

    function validationPage($page)
    {
        if ($page <= 0) {
            return [false, "Отрицательное или нулевое значение страницы."];
        } elseif (!ctype_digit($page)) {
            return [false, "Нечисловые символы в ссылке."];
        } elseif ($page >= 10000000) {
            return [false, "Слишком большая страница."];
        } else {
            return [true];
        }
    }
