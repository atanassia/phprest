<?php

require_once __DIR__.'/router.php';

// Static GET
// In the URL -> http://localhost
// The output -> Index
// get('/', 'views/index.php');

// Dynamic GET. Example with 1 variable
// The $id will be available in user.php
// get('/user/$id', 'views/user');

// Dynamic GET. Example with 2 variables
// The $name will be available in full_name.php
// The $last_name will be available in full_name.php
// In the browser point to: localhost/user/X/Y
// get('/user/$name/$last_name', 'views/full_name.php');

// Dynamic GET. Example with 2 variables with static
// In the URL -> http://localhost/product/shoes/color/blue
// The $type will be available in product.php
// The $color will be available in product.php
// get('/product/$type/color/$color', 'product.php');



get('/healthcheck', 'views/healthcheck.php');

get('/card/page/$page', 'views/card/getAllCards.php');
get('/card/$id', 'views/card/getSingleCard.php');

post('/card/user/$username', 'views/card/gerAllUserCards.php');
post('/card', 'views/card/createCard.php');
delete('/card/$id', 'views/card/deleteCard.php');
put('/card/$id', 'views/card/updateCard.php');


get('/user/$page', 'views/user/getAllUsers.php');
get('/user/id/$id', 'views/user/getSingleUser.php');

get('/user/find/$username', 'views/user/findUsers.php');

post('/user', 'views/user/createUser.php');
delete('/user/$id', 'views/user/deleteUser.php');
put('/user/$id', 'views/user/updateUser.php');



// ##################################################
// any can be used for GETs or POSTs
// For GET or POST
// The 404.php which is inside the views folder will be called
// The 404.php has access to $_GET and $_POST
any('/404', 'views/404.php');
