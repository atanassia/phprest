<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function getAllUsers($page)
    {
        if (!validationPage($page)[0]) {
            return returnMessage("error", validationPage($page)[1]);
        }

        $database = new Database();
        $db = $database->getConnection();
        $items = new User($db);

        $stmt = $items->getAllUsers($page);
        $itemCount = $stmt->rowCount();

        if ($itemCount > 0) {
            $dataArr = array();
            $dataArr["body"] = array();
            $dataArr["pageNumber"] = $page;

            $data = $stmt->fetchAll();
            foreach($data as $row){
                $arr = array(
                    "id" => $row["id"],
                    "username" => $row["username"],
                    "created" => $row["created"],
                    "updated" => $row["updated"]
                );
                array_push($dataArr["body"], $arr);
            }
            return json_encode($dataArr, JSON_UNESCAPED_UNICODE);
        } else {
            // http_response_code(404);
            return returnMessage("message", "Нет записей.");
        }
    }

    header("Content-Type: application/json");
    echo getAllUsers($page);
