<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function deleteUser($id)
    {
        $database = new Database();
        $db = $database->getConnection();

        $item = new User($db);
        $item->id = $id;

        if ($item->deleteUser() != 0) {
            return returnMessage("error", "Пользователь удален.");
        }

        return returnMessage("error", "Такого пользователя не существует.");
    }

    header("Content-Type: application/json");
    echo deleteUser($id);
