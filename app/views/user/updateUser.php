<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function updateUser($id)
    {
        $database = new Database();
        $db = $database->getConnection();

        $item = new User($db);

        $data = json_decode(file_get_contents("php://input"));

        $item->id = $id;
        $item->username = $data->username;

        if (!empty($item->username)) {
            if ($item->updateUser() != 0) {
                return returnMessage("message", "Пользователь обновлен.");
            } else {
                return returnMessage("error", "Не получается обновить запись.");
            }
        } else {
            return returnMessage("error", "Пусто поле username.");
        }
    }

    header("Content-Type: application/json");
    echo updateUser($id);
