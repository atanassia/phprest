<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function getSingleUser($id)
    {
        $database = new Database();
        $db = $database->getConnection();
        $item = new User($db);

        $item->id = $id;
        $item->getUserById();

        if ($item->username) {
            $emp_arr = array(
                "id" =>  $item->id,
                "username" => $item->username,
                "created" => $item->created,
                "updated" => $item->updated
            );
            return json_encode($emp_arr, JSON_UNESCAPED_UNICODE);
        } else {
            http_response_code(404);
            return returnMessage("message", "Пользователь не найден.");
        }
    }

    header("Content-Type: application/json");
    echo getSingleUser($id);
