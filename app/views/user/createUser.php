<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function createUser()
    {
        $database = new Database();
        $db = $database->getConnection();

        $item = new User($db);
        $data = json_decode(file_get_contents("php://input"));

        $item->username = $data->username;

        if (!empty($item->username)) {
            try {
                $res = $item->createUser();
                if ($res!= 0) {
                    return returnMessage("message", "Пользователь добавлен.");
                } else {
                    return returnMessage("error", "Не получается создать пользователя.");
                }
            } catch(PDOException $e) {
                return returnMessage("error", getErrorMessage($e->errorInfo));
            }
        } else {
            return returnMessage("error", "Пустое поле username.");
        }
    }

    header("Content-Type: application/json");
    echo createUser();
