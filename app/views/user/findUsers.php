<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function findUsers($username)
    {
        $database = new Database();
        $db = $database->getConnection();
        $items = new User($db);

        $items -> username = $username;
        $stmt = $items->findByUsername();
        $itemCount = $stmt->rowCount();

        if ($itemCount > 0) {
            $dataArr = array();
            $dataArr["body"] = array();
            $dataArr["itemCount"] = $itemCount;

            $data = $stmt->fetchAll();
            foreach($data as $row){
                $arr = array(
                    "id" => $row["id"],
                    "username" => $row["username"],
                    "created" => $row["created"],
                    "updated" => $row["updated"]
                );
                array_push($dataArr["body"], $arr);
            }
            return json_encode($dataArr, JSON_UNESCAPED_UNICODE);
        } else {
            // http_response_code(404);
            return returnMessage("message", "Нет записей.");
        }
    }

    header("Content-Type: application/json");
    echo findUsers(urldecode($username));
