<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/card.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function createCard()
    {
        $database = new Database();
        $db = $database->getConnection();

        $cardItem = new Card($db);
        $userItem = new User($db);

        $data = json_decode(file_get_contents("php://input"));

        $userItem->username = $data->username;
        $userItem->getUserByUsername();

        if ($userItem->id) {
            $cardItem -> user_id = $userItem->id;


            $itemCard = $cardItem -> getAllUserCards();

            if ($itemCard -> rowCount() >= 5) {
                return returnMessage("error", "Данный пользователь имеет максимально допустимое кол-во карт (5).");
            }
        } else {
            return returnMessage("error", "Пользователя с таким именем не существует.");
        }

        $cardItem->card = $data->card;
        $message = $cardItem->checkCard();

        if ($message == 'Номер карты правильный.') {
            try {
                if ($cardItem->createCard()!= 0) {
                    return returnMessage("message", "Карта добавлена.");
                } else {
                    return returnMessage("error", "Не получается создать карту.");
                }
            } catch(PDOException $e) {
                return returnMessage("error", getErrorMessage($e->errorInfo));
            }
        } else {
            return returnMessage("error", $message);
        }
    }

    header("Content-Type: application/json");
    echo createCard();
