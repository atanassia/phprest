<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/card.php';
    require_once '/var/www/html/helpers.php';

    function getSingleCard($id)
    {
        $database = new Database();
        $db = $database->getConnection();
        $item = new Card($db);

        $item->id = $id;
        $item->getCard();

        if ($item->card) {
            $emp_arr = array(
                "id" =>  $item->id,
                "card" => $item->card,
                "created" => $item->created,
                "updated" => $item->updated
            );
            return json_encode($emp_arr);
        } else {
            // http_response_code(404);
            return returnMessage("message", "Карта не найдена.");
        }
    }

    header("Content-Type: application/json");
    echo getSingleCard($id);
