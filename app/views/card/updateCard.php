<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/card.php';
    require_once '/var/www/html/helpers.php';

    function updateCard($id)
    {
        $database = new Database();
        $db = $database->getConnection();

        $item = new Card($db);

        $data = json_decode(file_get_contents("php://input"));

        $item->id = $id;
        $item->card = $data->card;

        $message =  $item->checkCard();

        if ($message == 'Номер карты правильный.') {
            if ($item->updateCard() != 0) {
                return returnMessage("message", "Карта обновлена.");
            } else {
                return returnMessage("error", "Не получается обновить карту.");
            }
        } else {
            return returnMessage("error", $message);
        }
    }

    header("Content-Type: application/json");
    echo updateCard($id);
