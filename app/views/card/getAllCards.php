<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/card.php';
    require_once '/var/www/html/helpers.php';

    function getAllCards($page)
    {
        if (!validationPage($page)[0]) {
            return returnMessage("error", validationPage($page)[1]);
        }

        $database = new Database();
        $db = $database->getConnection();

        $items = new Card($db);

        $stmt = $items->getAllCards($page[0]);
        $itemCount = $stmt->rowCount();

        if ($itemCount > 0) {
            $dataArr = array();
            $dataArr["body"] = array();
            $dataArr["pageNumber"] = $page;

            $data = $stmt->fetchAll();
            foreach($data as $row){
                $arr = array(
                    "id" => $row["id"],
                    "card" => $row["card"],
                    "created" => $row["created"],
                    "updated" => $row["updated"]
                );
                array_push($dataArr["body"], $arr);
            }
            return json_encode($dataArr, JSON_UNESCAPED_UNICODE);
        } else {
            http_response_code(404);
            return returnMessage("message", "Нет записей.");
        }
    }

    header("Content-Type: application/json");
    echo getAllCards($page);
