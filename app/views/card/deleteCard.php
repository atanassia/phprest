<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/card.php';
    require_once '/var/www/html/helpers.php';

    function deleteCard($id)
    {
        $database = new Database();
        $db = $database->getConnection();

        $item = new Card($db);
        $item->id = $id;

        if ($item->deleteCard() !== 0) {
            return returnMessage("error", "Карта $id удалена.");
        }
        return returnMessage("error", "Карты не существует.");
    }

    header("Content-Type: application/json");
    echo deleteCard($id);
