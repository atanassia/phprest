<?php

    require_once '/var/www/html/config/database.php';
    require_once '/var/www/html/class/card.php';
    require_once '/var/www/html/class/user.php';
    require_once '/var/www/html/helpers.php';

    function getAllUserCards($username)
    {
        $database = new Database();
        $db = $database->getConnection();

        $cardItem = new Card($db);
        $userItem = new User($db);

        $userItem->username = $username;
        $userItem->getUserByUsername();


        if ($userItem->id) {
            $cardItem -> user_id = $userItem->id;

            $stmt = $cardItem->getAllUserCards();
            $itemCount = $stmt->rowCount();

            if ($itemCount > 0) {
                $dataArr = array();
                $dataArr["body"] = array();
                $dataArr["itemCount"] = $itemCount;

                $data = $stmt->fetchAll();
                foreach($data as $row){
                    $arr = array(
                        "id" => $row["id"],
                        "card" => $row["card"],
                        "created" => $row["created"],
                        "updated" => $row["updated"]
                    );
                    array_push($dataArr["body"], $arr);
                }
                return json_encode($dataArr, JSON_UNESCAPED_UNICODE);
            } else {
                http_response_code(404);
                return returnMessage("message", "Нет записей.");
            }
        } else {
            return returnMessage("error", "Пользователя с таким именем не существует.");
        }
    }

    header("Content-Type: application/json");
    echo getAllUserCards(urldecode($username));
