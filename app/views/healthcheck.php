<?php

    function healthcheck()
    {
        $emp_arr = array(
            "status" =>  'я живой'
            );

        http_response_code(200);
        return json_encode($emp_arr, JSON_UNESCAPED_UNICODE);
    }

    header("Content-Type: application/json");
    echo healthcheck();
