<?php

    class User
    {
        private $conn;
        private $db_table = "User";
        public $id;
        public $username;
        public $created;
        public $updated;


        public function __construct($db)
        {
            $this->conn = $db;
        }

        // Get all users
        public function getAllUsers($page)
        {
            $sqlQuery = "
                SELECT * 
                FROM " . $this->db_table . "
                LIMIT 5 OFFSET :offset";
            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindValue(":offset", offset($page), PDO::PARAM_INT);

            $stmt->execute();
            return $stmt;
        }

        public function findByUsername()
        {
            $sqlQuery = "
                SELECT * 
                FROM " . $this->db_table . "
                WHERE MATCH (username) AGAINST (?)";
            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindValue(1, $this -> username);
            $stmt->execute();
            return $stmt;
        }

        public function getUserByUsername()
        {
            $sqlQuery = "
                    SELECT id
                    FROM ". $this->db_table ."
                    WHERE username = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->username);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = ($dataRow) ? $this->id = $dataRow['id'] : $this->id = null;
        }

        public function getUserById()
        {
            $sqlQuery = "
                    SELECT *
                    FROM ". $this->db_table ."
                    WHERE id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($dataRow) {
                $this->id = $dataRow['id'];
                $this->username = $dataRow['username'];
                $this->created = $dataRow['created'];
                $this->updated = $dataRow['updated'];
            } else {
                $this->username = null;
            }
        }

        // Create user
        public function createUser()
        {
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                    username = :username";

            $stmt = $this->conn->prepare($sqlQuery);

            $this->username=htmlspecialchars(strip_tags($this->username));
            $stmt->bindParam(":username", $this->username);
            $stmt->execute();

            return $stmt->rowCount();
        }

        // Update user
        public function updateUser()
        {
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        username = :username
                    WHERE 
                        id = :id";

            $stmt = $this->conn->prepare($sqlQuery);

            $this->username=htmlspecialchars(strip_tags($this->username));

            $stmt->bindParam(":username", $this->username);
            $stmt->bindParam(":id", $this->id);

            $stmt->execute();
            return $stmt->rowCount();
        }

        // Delete user
        public function deleteUser()
        {
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);

            $this->id=htmlspecialchars(strip_tags($this->id));

            $stmt->bindParam(1, $this->id);

            $stmt->execute();
            return $stmt->rowCount();
        }
    }
