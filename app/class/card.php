<?php

    require_once '/var/www/html/helpers.php';

    class Card
    {
        private $conn;
        private $db_table = "Card";
        public $id;
        public $card;
        public $user_id;
        public $created;
        public $updated;


        public function __construct($db)
        {
            $this->conn = $db;
        }

        // Get all cards
        public function getAllCards($page)
        {
            $sqlQuery = "
                    SELECT *
                    FROM " . $this->db_table . "
                    ORDER BY id ASC
                    LIMIT 5 OFFSET :offset";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindValue(":offset", offset($page), PDO::PARAM_INT);

            $stmt->execute();
            return $stmt;
        }

        public function getAllUserCards()
        {
            $sqlQuery = "
                    SELECT *
                    FROM ". $this->db_table ."
                    WHERE user_id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->user_id);
            $stmt->execute();

            return $stmt;
        }

        // Get single card by id
        public function getCard()
        {
            $sqlQuery = "
                    SELECT *
                    FROM ". $this->db_table ."
                    WHERE id = ?";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($dataRow) {
                $this->id = $dataRow['id'];
                $this->card = $dataRow['card'];
                $this->created = $dataRow['created'];
                $this->updated = $dataRow['updated'];
            } else {
                $this->card = null;
            }
        }

        // Create card
        public function createCard()
        {
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        user_id = :user_id,
                        card = :card";

            $stmt = $this->conn->prepare($sqlQuery);

            $this->card=str_replace(' ', '', htmlspecialchars(strip_tags($this->card)));
            $stmt->bindParam(":card", $this->card);
            $stmt->bindParam(":user_id", $this->user_id);

            $stmt->execute();
            return $stmt->rowCount();
        }

        // Update card
        public function updateCard()
        {
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        card = :card
                    WHERE 
                        id = :id";

            $stmt = $this->conn->prepare($sqlQuery);

            $this->card=str_replace(' ', '', htmlspecialchars(strip_tags($this->card)));

            $stmt->bindParam(":card", $this->card);
            $stmt->bindParam(":id", $this->id);

            $stmt->execute();
            return $stmt->rowCount();
        }

        // Delete card
        public function deleteCard()
        {
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);

            $this->id=htmlspecialchars(strip_tags($this->id));

            $stmt->bindParam(1, $this->id);

            $stmt->execute();
            return $stmt->rowCount();
        }
        public function luna()
        {
            $sum = 0;
            $number = str_replace(' ', '', $this->card);
            $len = strlen($number);
            for ($i = 0; $i < $len; $i++) {
                if ((($i + 1) % 2) == 0) {
                    $val = $number[$i];
                } else {
                    $val = $number[$i] * 2;
                    if ($val > 9) {
                        $val -= 9;
                    }
                }
                $sum += $val;
            }
            return (($sum % 10) === 0);
        }

        public function card_type()
        {
            if (strlen($this->card) == 14 && substr($this->card, 0, 6) == "148199") {
                return true;
            } else {
                if ($this->card[0] == "4" || $this->card[0] == "5") {
                    return true;
                }
            }
            return false;
        }

        // Card check
        public function checkCard()
        {
            $message = '';

            $card = str_replace(' ', '', $this->card);
            if (strlen($card) == 0) {
                $message = 'Нет данных карты.';
            } else {
                if ($this -> card_type()) {
                    if ($this -> luna()) {
                        $message = 'Номер карты правильный.';
                    } else {
                        $message = 'Номер карты непарвильный.';
                    }
                } else {
                    $message = 'Тип карты неправильный.';
                }
            }

            return $message;
        }
    }
