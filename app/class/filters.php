<?php

    class MetaData
    {
        public $currentPage;

        public $pageSize;
        public $lastPage;
        public $totalRecords;
    }

    class Filter
    {
        public $page;
        public $pageSize;
        public $sort;
        public $sortSafeList;


        public function sortColumn()
        {
            foreach ($this -> sortSafeList as $value) {
                if ($this -> sort == $value) {
                    return ltrim($this -> sort, "-");
                }
            }
        }

        public function sortDirection()
        {
            if (str_starts_with($this -> sort, '-')) {
                return "DESC";
            }
            return "ASC";
        }

        public function limit()
        {
            return $this -> pageSize;
        }

        public function offset()
        {
            return ($this -> page - 1) * $this -> pageSize;
        }
    }
